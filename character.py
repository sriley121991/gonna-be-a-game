class Character(object):
    """Parent for all characters, including players"""
    def __init__(self, race, attack, hit_points, magic_points, level):
        self.race = race
        self.attack = attack
        self.hit_points = hit_points
        self.magic_points = magic_points
        self.level = level

        